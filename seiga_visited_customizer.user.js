// ==UserScript==
// @name         Seiga visited customizer
// @namespace    https://javelin.works
// @version      0.1.0
// @description  ニコニコ静画の訪問済みリンクをカスタマイズ
// @author       sanadan <jecy00@gmail.com>
// @match        https://seiga.nicovideo.jp/*
// @license      MIT
// @grant        none
// ==/UserScript==

(function () {
  'use strict'

  document.head.insertAdjacentHTML('beforeend', `
<style>
a[href^="/seiga/"] > div:nth-child(1),
a[href^="/comic/"] img,
#my_personalize div.center_img a.center_img_inner,
.list_item.middle > a > .thum,
.list_item_cutout.large > a,
a[href^="/seiga"] > img {
  box-sizing: content-box;
  border: 1px solid white;
}
a[href^="/seiga/"]:visited > div:nth-child(1),
a[href^="/comic/"]:visited img,
#my_personalize div.center_img a.center_img_inner:visited,
.list_item.middle > a:visited > .thum,
.list_item_cutout.large > a:visited,
a[href^="/seiga"]:visited > img,
#mg_main_column #mg_primary #mg_recently_commented .content_area_inner li a.mg_thumb_img:visited,
#mg_main_column #mg_primary #mg_feature .content_area_inner #mg_feature_main .mg_thumb_img_wrapper a.mg_thumb_img:visited,
#mg_main_column #mg_primary #mg_feature .content_area_inner ul li a.mg_thumb_img:visited,
#mg_main_column #mg_primary #mg_recent #mg_recent_inner ul.mg_recent_content li a.mg_thumb_img:visited,
#mg_main_column #mg_secondary #mg_favorite .content_area_inner .mg_favorite_box .mg_thumb_img_wrapper a:visited {
  border-color: green;
}
a[href^="/seiga/"]:visited span.popular_illust_block__item__info--title,
#top_popular_manga_box .popular_manga_block__item__info--title a:visited,
#top_personalize_box--manga .top_personalize_block__body__item--info-content_title a:visited,
a[href^="/comic/"]:visited,
#my_personalize .illust_info a:visited,
.list_item.middle a:visited .illust_info .title {
  color: green;
}
#manga_pickup_area .pickup_item .item_thumbnail img,
#mg_main_column #mg_primary #mg_recently_commented .content_area_inner li .mg_thumb_img img,
#mg_main_column #mg_primary #mg_feature .content_area_inner #mg_feature_main .mg_thumb_img_wrapper .mg_thumb_img img,
#mg_main_column #mg_primary #mg_feature .content_area_inner ul li .mg_thumb_img img,
#mg_main_column #mg_primary #mg_recent #mg_recent_inner ul.mg_recent_content li .mg_thumb_img img,
#mg_main_column #mg_secondary #mg_favorite .content_area_inner .mg_favorite_box .mg_thumb_img_wrapper a img,
#mg_main_column #mg_secondary #mg_new .mg_thumb_img_wrapper a img {
  border-width: 0;
}
</style>
`)
})()
